import org.kde.kirigami 2.0
import QtQuick 2.0

ApplicationWindow
{
    id: window

    pageStack.initialPage: Page {
        Label {
            id: pageText
            anchors.centerIn: parent
            text: "Hola"
        }
        actions.contextualActions: [
            Action {
                text: "Change"
                onTriggered: pageText.text = "Olá"
            },
            Action {
                text: "Change More"
                onTriggered: pageText.text = "Olá 2"
            },
            Action {
                text: "Change More 2"
                onTriggered: pageText.text = "Olá 2"
            }
        ]
    }

    contextDrawer: ContextDrawer {}
    globalDrawer: GlobalDrawer {
        title: "Example 4"
        bannerImageSource: "banner.svg"
    }
}
