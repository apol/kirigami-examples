import org.kde.kirigami 2.1
import QtQuick 2.5

ApplicationWindow
{
    id: window

    Component {
        id: rectComp
        Page {
            Rectangle {
                color: Qt.hsla(Math.random(),1,.2);
                anchors.fill: parent
            }
        }
    }

    pageStack.initialPage: ScrollablePage {
        ListView {
            headerPositioning: ListView.OverlayHeader
            header: ItemViewHeader {
                backgroundImage.source: "https://images.unsplash.com/photo-1456132311779-ca4ff6130510?dpr=1&auto=format&fit=crop&w=1080&h=720&q=80&cs=tinysrgb&crop="
                title: "My favorite letters"
            }

            model: ['a', 'b', 'c', 'd', '€', 'f', 'g', 'h', 'i']
            delegate: BasicListItem {
                label: modelData
                onClicked: window.pageStack.push(rectComp)
            }
        }
    }

    globalDrawer: GlobalDrawer {
        title: "Example 3"
        bannerImageSource: "banner.svg"
    }
}
