# Kirigami examples

This repository is meant to contain several simple examples of Kirigami applications.

It is supposed to provide easy ways to introduce components without being overwhelming.

All of them can be opened separately with:
```
qml exampleX.qml
```
