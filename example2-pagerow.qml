import org.kde.kirigami 2.0
import QtQuick 2.0

ApplicationWindow
{
    id: window

    Component {
        id: rectComp
        Page {
            property alias color: rect.color
            Rectangle {
                id: rect
                anchors.fill: parent
            }
        }
    }

    pageStack.initialPage: Page {
        Label {
            anchors.centerIn: parent
            text: "Olá"
        }
    }

    globalDrawer: GlobalDrawer {
        title: "Example 2"
        bannerImageSource: "banner.svg"
        actions: [
            Action {
                text: "Azul"
                onTriggered: window.pageStack.push(rectComp, {color: "blue"})
            },
            Action {
                text: "Vermelho"
                onTriggered: window.pageStack.push(rectComp, {color: "red"})
            },
            Action {
                text: "Clean"
                onTriggered: window.pageStack.clear()
            }
        ]
    }
}
