import org.kde.kirigami 2.0

ApplicationWindow
{
    id: window
    globalDrawer: GlobalDrawer {
        title: "Example 1"
        bannerImageSource: "banner.svg"
        actions: [
            Action {
                text: "Coisa A"
                onTriggered: window.showPassiveNotification("Olá A")
            },
            Action {
                text: "Coisa B"
                onTriggered: window.showPassiveNotification("Olá B")
            },
            Action {
                text: "Coisa C"
                onTriggered: window.showPassiveNotification("Olá C")
            }
        ]
    }
}
